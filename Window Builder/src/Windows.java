import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JColorChooser;
import java.awt.Color;
import java.awt.Panel;
import java.awt.Label;
import java.awt.Button;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.TextField;

public class Windows extends JFrame {

	/**
	 * A Anton creation
	 */
	private static final long serialVersionUID = 2;
	private JPanel cp_Hintergrund;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Windows frame = new Windows();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.A Anton creation
	 */
	public Windows() {
		
		JFrame frame = new JFrame("JColorChooser Sample Popup");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 384, 583);
		cp_Hintergrund = new JPanel();
		cp_Hintergrund.setBackground(new Color(240, 248, 255));
		cp_Hintergrund.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(cp_Hintergrund);
		cp_Hintergrund.setLayout(null);
		
		Label label_info_H = new Label("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		label_info_H.setBounds(10, 65, 190, 22);
		cp_Hintergrund.add(label_info_H);
		
		Button btn_Gruen = new Button("Gr\u00FCn");

		btn_Gruen.setBackground(new Color(135, 206, 250));
		btn_Gruen.setBounds(127, 93, 111, 22);
		cp_Hintergrund.add(btn_Gruen);
		
		Button btn_Rot = new Button("Rot");

		btn_Rot.setBackground(new Color(135, 206, 250));
		btn_Rot.setBounds(10, 93, 111, 22);
		cp_Hintergrund.add(btn_Rot);
		
		Button btn_Blau = new Button("Blau");

		btn_Blau.setBackground(new Color(135, 206, 250));
		btn_Blau.setBounds(244, 93, 114, 22);
		cp_Hintergrund.add(btn_Blau);
		
		Button btn_Gelb = new Button("Gelb");

		btn_Gelb.setBackground(new Color(135, 206, 250));
		btn_Gelb.setBounds(10, 121, 111, 22);
		cp_Hintergrund.add(btn_Gelb);
		
		Button btn_Standart_Farbe = new Button("Standartfarbe");

		btn_Standart_Farbe.setBackground(new Color(135, 206, 250));
		btn_Standart_Farbe.setBounds(127, 121, 111, 22);
		cp_Hintergrund.add(btn_Standart_Farbe);
		
		Button btn_Farbe_Auswahl = new Button("Farbe w\u00E4hlen");

		btn_Farbe_Auswahl.setBackground(new Color(135, 206, 250));
		btn_Farbe_Auswahl.setBounds(244, 121, 114, 22);
		cp_Hintergrund.add(btn_Farbe_Auswahl);
		
		Label label_info_T = new Label("Aufgabe 2: Text formatieren");
		label_info_T.setBounds(10, 149, 190, 22);
		cp_Hintergrund.add(label_info_T);
		
		Button btn_Arial = new Button("Arial");

		btn_Arial.setBackground(new Color(135, 206, 250));
		btn_Arial.setBounds(10, 177, 111, 22);
		cp_Hintergrund.add(btn_Arial);
		
		Button btn_Comic = new Button("Comic Sans MS");
		btn_Comic.setFont(new Font("Arial", Font.PLAIN, 12));

		btn_Comic.setBackground(new Color(135, 206, 250));
		btn_Comic.setBounds(127, 177, 111, 22);
		cp_Hintergrund.add(btn_Comic);
		
		Button btn_Courier = new Button("Courier New");

		btn_Courier.setBackground(new Color(135, 206, 250));
		btn_Courier.setBounds(244, 177, 114, 22);
		cp_Hintergrund.add(btn_Courier);
		
		Button btn_Label_S = new Button("ins Label schreiben");

		btn_Label_S.setBackground(new Color(135, 206, 250));
		btn_Label_S.setBounds(10, 233, 171, 22);
		cp_Hintergrund.add(btn_Label_S);
		
		Button btn_Label_L = new Button("Text im Label l\u00F6schen");

		btn_Label_L.setBackground(new Color(135, 206, 250));
		btn_Label_L.setBounds(187, 233, 171, 22);
		cp_Hintergrund.add(btn_Label_L);
		
		Label label_info_4 = new Label("Aufgabe 3: Schriftfarbe \u00E4ndern");
		label_info_4.setBounds(10, 261, 190, 22);
		cp_Hintergrund.add(label_info_4);
		
		Button btn_Schrift_Rot = new Button("Rot");

		btn_Schrift_Rot.setBackground(new Color(135, 206, 250));
		btn_Schrift_Rot.setBounds(10, 289, 114, 22);
		cp_Hintergrund.add(btn_Schrift_Rot);
		
		Button btn_Schrift_Schwarz = new Button("Schwarz");
		btn_Schrift_Schwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_Schrift_Schwarz.setBackground(new Color(135, 206, 250));
		btn_Schrift_Schwarz.setBounds(244, 289, 114, 22);
		cp_Hintergrund.add(btn_Schrift_Schwarz);
		
		Button btn_Schrift_Blau = new Button("Blau");

		btn_Schrift_Blau.setBackground(new Color(135, 206, 250));
		btn_Schrift_Blau.setBounds(127, 289, 111, 22);
		cp_Hintergrund.add(btn_Schrift_Blau);
		
		Label label_info_5 = new Label("Aufgabe4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		label_info_5.setBounds(10, 317, 190, 22);
		cp_Hintergrund.add(label_info_5);
		
		Button btn_Plus = new Button("+");

		btn_Plus.setBackground(new Color(135, 206, 250));
		btn_Plus.setBounds(10, 345, 171, 22);
		cp_Hintergrund.add(btn_Plus);
		
		Button btn_Minus = new Button("-");

		btn_Minus.setBackground(new Color(135, 206, 250));
		btn_Minus.setBounds(187, 345, 171, 22);
		cp_Hintergrund.add(btn_Minus);
		
		Label label_info_6 = new Label("Aufgabe 5: Textausrichtung");
		label_info_6.setBounds(10, 373, 190, 22);
		cp_Hintergrund.add(label_info_6);
		
		Button btn_LinksbŁndig = new Button("linksb\u00FCndig");

		btn_LinksbŁndig.setBackground(new Color(135, 206, 250));
		btn_LinksbŁndig.setBounds(10, 401, 114, 22);
		cp_Hintergrund.add(btn_LinksbŁndig);
		
		Button btn_zenter = new Button("zentriert");

		btn_zenter.setBackground(new Color(135, 206, 250));
		btn_zenter.setBounds(127, 401, 114, 22);
		cp_Hintergrund.add(btn_zenter);
		
		Button btn_rechtsbŁndig = new Button("rechtsb\u00FCndig");

		btn_rechtsbŁndig.setBackground(new Color(135, 206, 250));
		btn_rechtsbŁndig.setBounds(244, 401, 114, 22);
		cp_Hintergrund.add(btn_rechtsbŁndig);
		
		Label label_info_7 = new Label("Aufgabe 6: Programm beenden");
		label_info_7.setBounds(10, 429, 190, 22);
		cp_Hintergrund.add(label_info_7);
		
		Button btn_Exit = new Button("Exit");
		btn_Exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btn_Exit.setBackground(new Color(135, 206, 250));
		btn_Exit.setBounds(10, 457, 348, 77);
		cp_Hintergrund.add(btn_Exit);
		
		TextField txtEingabe = new TextField();
		txtEingabe.setText("Hier bitte Text eingeben");
		txtEingabe.setBounds(10, 205, 348, 22);
		cp_Hintergrund.add(txtEingabe);
		
		JLabel lblText = new JLabel("Dieser Text soll veaendert werden.");
		lblText.setBounds(10, 25, 348, 22);
		cp_Hintergrund.add(lblText);
		
		btn_Gruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cp_Hintergrund.setBackground(new Color(0, 255, 0));
				lblText.setBackground(new Color(0,255,0));
				label_info_H.setBackground(new Color(0,255,0));
				label_info_T.setBackground(new Color(0,255,0));
				txtEingabe.setBackground(new Color(0,255,0));
				label_info_4.setBackground(new Color(0,255,0));
				label_info_5.setBackground(new Color(0,255,0));
				label_info_6.setBackground(new Color(0,255,0));
				label_info_7.setBackground(new Color(0,255,0));
			}
		});
		btn_Rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cp_Hintergrund.setBackground(new Color(255,0,0));
				lblText.setBackground(new Color(255,0,0));
				label_info_H.setBackground(new Color(255,0,0));
				label_info_T.setBackground(new Color(255,0,0));
				txtEingabe.setBackground(new Color(255,0,0));
				label_info_4.setBackground(new Color(255,0,0));
				label_info_5.setBackground(new Color(255,0,0));
				label_info_6.setBackground(new Color(255,0,0));
				label_info_7.setBackground(new Color(255,0,0));
			}
		});
		btn_Blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cp_Hintergrund.setBackground(new Color(0,0,255)); 
				lblText.setBackground(new Color(0,0,255));
				label_info_H.setBackground(new Color(0,0,255));
				label_info_T.setBackground(new Color(0,0,255));
				txtEingabe.setBackground(new Color(0,0,255));
				label_info_4.setBackground(new Color(0,0,255));
				label_info_5.setBackground(new Color(0,0,255));
				label_info_6.setBackground(new Color(0,0,255));
				label_info_7.setBackground(new Color(0,0,255));
			}
		});
		btn_Gelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cp_Hintergrund.setBackground(new Color(255,255,0));
				lblText.setBackground(new Color(255,255,0));
				label_info_H.setBackground(new Color(255,255,0));
				label_info_T.setBackground(new Color(255,255,0));
				txtEingabe.setBackground(new Color(255,255,0));
				label_info_4.setBackground(new Color(255,255,0));
				label_info_5.setBackground(new Color(255,255,0));
				label_info_6.setBackground(new Color(255,255,0));
				label_info_7.setBackground(new Color(255,255,0));
			}
		});
		btn_Standart_Farbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cp_Hintergrund.setBackground(new Color(240,248,255));
				lblText.setBackground(new Color(240,248,255));
				label_info_H.setBackground(new Color(240,248,255));
				label_info_T.setBackground(new Color(240,248,255));
				txtEingabe.setBackground(new Color(240,248,255));
				label_info_4.setBackground(new Color(240,248,255));
				label_info_5.setBackground(new Color(240,248,255));
				label_info_6.setBackground(new Color(240,248,255));
				label_info_7.setBackground(new Color(240,248,255));
			}
		});
		
		btn_Farbe_Auswahl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color background = JColorChooser.showDialog(null, "Change Button Background", getBackground());
				if (background != null) {
					cp_Hintergrund.setBackground(background);
					lblText.setBackground(background);
					label_info_H.setBackground(background);
					label_info_T.setBackground(background);
					txtEingabe.setBackground(background);
					label_info_4.setBackground(background);
					label_info_5.setBackground(background);
					label_info_6.setBackground(background);
					label_info_7.setBackground(background);
				}
			}
		});
		
		btn_Arial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		
		btn_Comic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		
		btn_Courier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Courier", Font.PLAIN, 12));
			}
		});
		
		btn_Label_S.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(txtEingabe.getText());
			}
		});
		btn_Label_L.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText("");
			}
		});
		
		btn_Schrift_Rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.RED);

			}
		});
		btn_Schrift_Blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLUE);
			}
		});
		
		btn_Schrift_Schwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLACK);
			}
		});
		btn_Plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblText.getFont().getSize();
				lblText.setFont(new Font(null, Font.PLAIN, groesse + 1));
			}
		});
		btn_Minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblText.getFont().getSize();
				lblText.setFont(new Font(null, Font.PLAIN, groesse - 1));
			}
		});
		
		btn_LinksbŁndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btn_zenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btn_rechtsbŁndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
	}
}
